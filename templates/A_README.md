# {Application Name}.
This is a {Mobile Application || Web Application || Desktop Application } developed for { devices running under Android operative system || devices running under iOS operative system || Custom }

## Development Team.
* [Team Member 1](https://gitlab.com/)
* [Team Member 2](https://gitlab.com/)
* [Team Member 3](https://gitlab.com/)

## How to collaborate?.

In order to collaborate on this project is necessary to review all the guidelines and steps defined on the [**Contributing Guide**](/CONTRIBUTING.md).

## Styleguides.

Inside the [**Contributing Guide**](/CONTRIBUTING.md) you can find more information about all the styleguides used in this project.

## API Definition.

In this project the API documentation is hosted on [Apiary](http://apiary.io/) to see the API specification you must review the [{Application Name} - Apiary](http://docs.applicationname.apiary.io/#)

In addition, you also could some of the API Rest request using Postman by clicking the next button.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/{collectionID}})
