# Operations

## Operations
Realizar una operación basada en software requiere más que código hermoso o un producto popular. Gestionar el flujo de efectivo y los impuestos puede sentirse sin importancia o no parecer difícil, pero conseguir que sean correctos es tan vital para nuestro éxito como el diseño del producto.

Afortunamente, existen muchos servicios los cuales hacen que cosas como contabilidad, recibos, firmas y facturación sean mucho más fáciles.

Los siguientes principios pueden ayudarnos a agilizar las operaciones:

* Hacer `Outsourcing` de cosas que son super importantes pero que no somos excelentes en su realización.
* Invertir tiempo seleccionando a un proveedor y ocasionalmente invertir tiempo reevaluando otros proveedores.
* `Automatizar` tareas repetitivas.
* Brindar a todos permisos de 'administrador' cuanto más sea posible para evitar `cuellos de botella`
* Evitar la contrucción de herramientas internas. Se requiere tiempo y dinero para construirlas y nos hace depender de nosotros mismos cuando las cosas no funcionan.
* Nuestros problemas no son únicos. Intentaremos procesos manuales al inicio.

## Correo electrónico

Intentamos no usar el correo electrónico, preferimos usar herramientas como `Slack`. Sin embargo, Gmail es nuestro campeón.

## Documents

Usamos Google Docs para nuestros documentos editables.

Preferimos Google Docs porque son:

* Fáciles de compartir mediante una URL. Todos cuentan con un navegador web, no todos tienen instalado MS-Office u OpenOffice.
* Siempre están actualizados con los últimos cambios.
* Permiten la colaboración en tiempo real, realizar comentarios y notas grupales.
* No debemos preocuparnos por realizar `backups`
* No cuestan.
* Se enfocan en documentación no tan extensa y hojas de cálculo no complicadas.

Nos enfocamos en generar solamente la documentación necesaria, por lo que no deberíamos escribir documentos largos.

En casos donde nos encontremos escribiendo documentos largos, es un buen momento para preguntarnos si es realmente necesario un análisis tan complicado.

Cuando los documentos son muy similares con ligeras variaciones (como los contratos), nosotros creamos `templates` usando Google Docs.

## Reuniones

Nos comunicamos excesivamente con los clientes en persona y en línea para reducir el número de reuniones programadas. Todos los problemas surgen de una mala comunicación.

Cuando necesitamos reunirnos para una discusión, buscamos 30 minutos en persona.

Cuando se trabaja de forma remota, Google Hangouts es indispensable como "la mejor opción". Compartir pantalla también es muy fácil, cuando sea necesario.