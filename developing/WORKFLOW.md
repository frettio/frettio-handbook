# Development Workflow

## Clean local workspace

![Update Local Repository](../assets/FRET_PR_ACTUALIZAR_REPOSITORIO_LOCAL_v2.0.png)

Before beginning to work on an issue the developer needs to ensure his local repository is up to date. If the developer is working on another task he must stash his changes before begin the cleaning.

```shell
$ git stash
```

For more information about stashing, we recommend to check [this thread](https://www.atlassian.com/git/tutorials/git-stash) about this topic.

First, is necessary to update his `develop` branch to match remote changes.

```shell
$ git fetch origin
```

```shell
$ git checkout develop
```

```shell
$ git pull origin develop
```

Second, he must update his remote references to branches:

```shell
$ git remote prune origin
```

Third, if the developer has local branches referencing non-existing remote branches, he must delete those useless branches because they are already part of the `develop` branch history. For each branch he need to execute:

```shell
$ git branch -d {branch-name}
```

Finally, if the developer has saved changes when I start the cleaning, he need to bring back those saved changes.

```shell
$ git stash apply
```

For more information about the last command, we recommend to check [this thread](http://stackoverflow.com/questions/4040717/git-remote-prune-didnt-show-as-many-pruned-branches-as-i-expected) about this topic.

## Work on an Issue

### Analyze the issue

After getting issues assigned, to work on an issue the developer must:
1. Click on the issues assigned to see full description.
2. Read the issue description.
  1. If there is any question, the developer needs to write a comment on the issue thread mentioning the current issues administrator.
  2. Wait for the clarifications.

### Create a working branch

In general, the name of the working branch needs to match with the issue identifier and title, following the next nomenclature:

    issueid-an-issue-title

Where:
* **issueid:** Is the identifier assigned by the platform where the administrator created the issue. Depending on the project software configuration this issue could be the reference to another identifier.
* **an-issue-title:** This value references the title field on the issue created on the software version control platform. Depending on the project software configuration this title may change.

*Examples of working branches names:*
* 3-user-profile-feature
* 4-verify-manifest-for-google-maps
* 2-review-history-of-user-locations
* etc

When the developer is ready to work on the issue, he needs to create a new branch. There is two ways of do this.

The easy way, is to click the button *New Branch* inside the issue detailed description. And after, the developer needs to create a local branch that points to the recently remote branch created by the platform.

```shell
$ git fetch origin
```

```shell
$ git checkout -b issueid-an-issue-title origin/issueid-an-issue-title
```

The second way, is to do it manually. The developer needs to create a local branch using the nomenclature defined at the beginning of this section and push it to the remote repository. *It is important that the branch starts from the develop remote reference.*

```shell
$ git fetch origin && git checkout develop
```

```shell
$ git checkout -b issueid-an-issue-title
```

```shell
$ git push origin issueid-an-issue-title
```

After the working branch is ready, the developer must make the proper changes and push his to work to the remote repository.

## Request Issue Review

When the developer has finished working on the assigned issue, and his working branch is already pushed to the remote repository, he needs to create a Pull Request (PR) from his working branch to *develop* branch and assign the administrator as a reviewer of the PR.

Inside the Pull Request description the developer can specify if the reviewer needs to read any comments made on the working branch commits. Besides, he can add proper labels to the PR.
