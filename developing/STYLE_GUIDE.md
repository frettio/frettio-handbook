Style Guide

We write code in a consistent style that emphasizes cleanliness and team communication.

High level guidelines:

Be consistent.
Don't rewrite existing code to follow this guide.
Don't violate a guideline without a good reason.
A reason is good when you can convince a teammate.
