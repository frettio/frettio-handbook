# Este es nuestro Handbook.

## Elegir Plataformas
Al principio de un proyecto, debemos decidir que plataformas usaremos. Elegir una plataforma depende de nuestras ideas para resolver los problemas de los usuarios. Después de considerar lo que es mejor para nuestros usuarios, elegiremos las herramientas que tengan una comunidad fuerte de respaldo, que nos hagan felices y nos ayuden a crear e iterar rápidamente.

##### Herramientas que hemos usado
* [Trello](tools/TRELLO.md)
* [GitLab](tools/GITLAB.md)
* [Slack](tools/SLACK.md)
* [Google Drive](tools/GOOGLE_DRIVE.md)

## Planeación
Una de nuestras principales metas es ejecutar procesos que generen pequeñas y frecuentes versiones del software funcional. Esto lo realizamos mediante comunicación frecuente e iteraciones semanales sobre un producto.

* [Los Procesos deben adaptarse a las necesidades del producto y del equipo](planning/PROCESSES.md)
* [Las juntas diarias generan confiancia y mantienen el impulso](planning/STANDUPS.md)
* [Nada supera la comunicación en persona](planning/PERSONAL_MESSAGE.md)
* [Administrar prioridades y visualizar el progreso con un proceso ligero](planning/EVERYONE_CAN_DO_IT.md)
* [Juntas semanales para discutir exitos, fallas y planes futuros](planning/WEEKLY_MEETING.md)

## Diseño
Nuestros proyectos están en constante cambio. Los diseñadores necesitan usar herramientas y seguir procesos apropiados para ese entorno de cambio constante.

* [¿Qué es Interaction Design?](designing/INTERACTION_DESIGN.md)
* [¿Qué es User Interface Design?](designing/UI_DESIGN.md)
* [¿Qué es Visual Design?](designing/VISUAL_DESIGN.md)

## Desarrollo
La mayoría de nuestras prácticas de desarrollo han sido detalladas en base a la experiencia, la ejecución iterativa y el aprendizaje que hemos obtenido a través de los años. Actualmente seguimos prácticas que se enfocan en optimizar los tiempos y la calidad de nuestro trabajo, sin afectar la feliidad de nuestro equipo de trabajo.

* [Artefactos](artifacts/README.md)
* [Flujo de Trabajo (Workflow)](developing/WORKFLOW.md)
* [Revisiones de Código (Code Reviews)](developing/CORE_REVIEWS.md)
* [Refactoring](developing/REFACTORING.md)
* [Guías de Estilo y Estándares (Style Guide)](developing/STYLE_GUIDE.md)
* [Control de Versiones (VCS)](developing/VERSION_CONTROL.md)

**Nota:**El trasfondo de nuestras prácticas se ve plasmado en el libro 'Extreme Programming Explained: Embrace Change' de Kent Beck y el libro 'The Psychology of Computer Programming' de Gerald Weinberg.

## Nuestra Compañia
Nostros creemos que siempre habrá una forma mejor de realizar nuestro trabajo, estamos dispuesto a encontrarla y compartirla con la mayor cantidad de gente posible.

* [Operaciones](company/OPERATIONS.md)
* [Principios](company/PRINCIPLES.md)
