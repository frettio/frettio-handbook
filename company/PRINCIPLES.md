# Principios

## Principio de No Dependencia

Regularmente eliminamos y simplificamos nuestras políticas. Nuestra política más importante es `Usa tu mejor juicio, pregunta sólo si lo necesitas y si no es algo trivial`.

## Minimizar las Jerarquías

Nos esforzamos por mantener pocos títulos de trabajo, pocos departamentos y pocas jerarquías. Preferimos la composición de los `roles necesarios` para los proyectos y los objetivos de la empresa sobre la herencia de los jefes e informes directos.

## Transparencia

`Evitamos tener conversaciones privadas` entre nosotros o con nuestros clientes. En cambio, hablamos en persona y usamos herramientas como Slack y GitLab para comunicarnos abiertamente dentro de un proyecto.

## Honestidad

Aunque debemos ser conscientes de los sentimientos de las personas y tratar de trabajar de forma constructiva, no podemos dejar que esas preocupaciones interfieran en nuestra felicidad y en el éxito de nuestro trabajo. Preferimos ser demasiado `honestos` que educados.

## Confianza

Nuestros estándares son muy altos, y traer a un nuevo miembro del equipo requiere un "sí" de todos los que participaron en el proceso de la entrevista. Por lo tanto, esperamos lo mejor de unos a otros, nos damos el beneficio de la duda, y nos alentamos a tomar la iniciativa de mejorar a nosotros mismos y la empresa.

## Mejora continua

Reconocemos que siempre podemos ser mejores. Por lo tanto, tenemos opiniones fuertes, sostenidas, y tomamos la iniciativa para mejorar nosotros mismos, la compañía, y nuestra comunidad.

## Piensa como si fueras un nuevo integrante

Independientemete de si realmente eres un nuevo integrante, o el miembro del equipo más antiguo, es importante no tomar nada por sentado o asumir que nuestra forma de realizar las cosas es universal. Nos enfocamos en crear procesos repetibles e independientes de quién los ejecute, con la documentación necesaria y sin ambigüedades.