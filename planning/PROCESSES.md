# Procesos

Los procesos necesitan adaptarse a las necesidades del producto y del equipo.

Actualmente nuestros procesos siguen una base iterativa y están divididos en 4 grandes categorías:

1. Administrativos.
2. Planeación.
3. Ejecución.
4. Cierre.

#### Nuestro Scratchpad

## Administrativos

* [Aclaración de Dudas](https://drive.google.com/file/d/0B4NjOclVm63dcnplWG9VbEFXa2s/view?usp=sharing)
* [Adminstración de Proyectos](https://drive.google.com/file/d/0B4NjOclVm63dMmFsWnNUZmFJQnM/view?usp=sharing)
* [Administración de Riesgos](https://drive.google.com/file/d/0B4NjOclVm63dTE5OakhtYUFhZEU/view?usp=sharing)
* [Desarrollo de Requisitos](https://drive.google.com/file/d/0B4NjOclVm63dbDBoTjlEUGlvVzg/view?usp=sharing)
* [Inicio del Proyecto](https://drive.google.com/file/d/0B4NjOclVm63dTkg3NzRTb0lZSzQ/view?usp=sharing)
* [Monitoreo y Control](https://drive.google.com/file/d/0B4NjOclVm63dYlBYbzRMSWM3RWM/view?usp=sharing)
* [Medición y Análisis](https://drive.google.com/file/d/0B4NjOclVm63dNmVDU3VUZFBYX00/view?usp=sharing)
* [Aseguramiento de la Calidad](https://drive.google.com/file/d/0B4NjOclVm63dZnZ4aVR0MFJzc3M/view?usp=sharing)

## Planeación

* [Asignación de Tareas](https://drive.google.com/file/d/0B4NjOclVm63dZXpzSmdndmoyTkk/view?usp=sharing)
* [Crear Listado de Historias de Usuario](https://drive.google.com/file/d/0B4NjOclVm63dWkpFSG0yS1hBVjg/view?usp=sharing)
* [Definir Criterios de Aceptación](https://drive.google.com/file/d/0B4NjOclVm63dbHNtb0JFaXQxc2M/view?usp=sharing)
* [Estimar Historias de Usuario](https://drive.google.com/file/d/0B4NjOclVm63dODh5bW9BVG5fWGM/view?usp=sharing)
* [Planeación del Proyecto](https://drive.google.com/file/d/0B4NjOclVm63daWFNWmtlLTVjREk/view?usp=sharing)
* [Planeación de Iteración](https://drive.google.com/file/d/0B4NjOclVm63dNzZ4VUZfNllKdzQ/view?usp=sharing)
* [Priorizar Historias de Usuario](https://drive.google.com/file/d/0B4NjOclVm63dNzZ4VUZfNllKdzQ/view?usp=sharing)

## Ejecución

* [Codificar una Historia de Usuario](https://drive.google.com/file/d/0B4NjOclVm63dVVliZFBFcHU2RUU/view?usp=sharing)
* [Diseñar Casos de Prueba](https://drive.google.com/file/d/0B4NjOclVm63dR3RVcWFPd2ZNWm8/view?usp=sharing)
* [Diseñar Mockups](https://drive.google.com/file/d/0B5cccA6IX8nBaTlfazh5a3daYW8/view?usp=sharing)
* [Ejecución de Iteración](https://drive.google.com/file/d/0B4NjOclVm63dc2RmZDViYmRkZ3M/view?usp=sharing)
* [Ejecutar Casos de Prueba](https://drive.google.com/file/d/0B4NjOclVm63dcFJ4dWxOUm80UGs/view?usp=sharing)
* [Especificación Detallada](https://drive.google.com/file/d/0B04anNPkCXG7X0N4TjFXYlp5aGs/view?usp=sharing)
* [Generar Diagramas UML](https://drive.google.com/file/d/0B4NjOclVm63dX3Y4THR1aG00dW8/view?usp=sharing)
* [Integración de Historia de Usuario](https://drive.google.com/file/d/0B4NjOclVm63dT3F4VFBId2owNGs/view?usp=sharing)
* [Actualizar Repositiorio Local](https://drive.google.com/file/d/0B4NjOclVm63dNUFkUHJ0N3pnbGs/view?usp=sharing)

## Cierre

* [Cierre de Iteración](https://drive.google.com/file/d/0B4NjOclVm63dY25BcGNpQmtsdXc/view?usp=sharing)
* [Mejora de Procesos](https://drive.google.com/file/d/0B5cccA6IX8nBQVNqNkZ2TEhpYm8/view?usp=sharing)
