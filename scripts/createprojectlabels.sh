#!/bin/bash

# Consider before run: chmod +x /path/to/script.sh
# How to run: ./createprojectlabels.sh PRIVATE-TOKEN projectIdentifer textFile.txt

private_key=$1
project_id=$2
file_text=$3

url="https://gitlab.com/api/v3/projects/$project_id/labels"
echo "Requesting to project "$project_id" with key "$private_key"... to URL "$url""

header=""PRIVATE-TOKEN:" "$private_key""

eval "touch "createlabelsoutput.json""

while IFS='' read -r line || [[ -n "$line" ]]; do
    IFS='&' read -r -a array <<< "$line"
    name="${array[0]}"
    color="${array[1]}"
    desc="${array[2]}"
    data="name=$name&color=$color&description=$desc"
    curlBody="--data \"$data\" --header \"$header\""

    curlCmd="curl -v $curlBody $url"
    eval $curlCmd >> createlabelsoutput.json
done < $file_text
