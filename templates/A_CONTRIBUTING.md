# Contributing to {Project Name}

The following is a set of guidelines for contributing to {Application Name} Application, which are hosted in the [{App Domain} Organization](www.google.com).

These are just guidelines, not strict rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

#### Table Of Contents

[What should I know before I get started?](#what-should-i-know-before-i-get-started)
  * [Architecture](#an-architecture)
  * [Third Party Libraries](#some-third-party-libs)

[How Can I Contribute?](#how-can-i-contribute)
  * [Development Workflow](#development-workflow)
  * [Reporting Bugs](#reporting-bugs)
  * [Using the GitLab board](#using-the-board)
  * [Pull Requests](#pull-requests)

[Styleguides](#styleguides)
  * [Git Commit Messages](#git-commit-messages)
  * [Android Styleguide](#javascript-styleguide)
  * [Documentation Styleguide](#documentation-styleguide)

[Additional Notes](#additional-notes)
  * [Issue and Pull Request Labels](#issue-and-pull-request-labels)

## What should I know before I get started?

### Architecture

For now, the project does not have a Software Architecture Document (SAD). We use a variant of Clean Architecture combined with the MVP pattern and the Repository Pattern. The major difference relies on the use of basic Interactors instead of Use Cases. We encourage the developer to research about this topic. However, here are some useful links:
* [MVP and Interactors by Platzi](https://www.youtube.com/watch?v=qWh1QlRpKxk)
* [Google Architecture Blueprint](https://github.com/googlesamples/android-architecture/tree/todo-mvp-clean/)
* [A Clean Architecture approach for Android](http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)
* [Clean Architecture by Uncle Bob](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)

### Third Party Libraries

We recommend to research, and even better, have experience on using all or some of the next third party libraries:
* [Retrofit](https://square.github.io/retrofit/)
* [Gson](https://github.com/google/gson)
* [Picasso](http://square.github.io/picasso/)
* [Pretty Time](https://github.com/ocpsoft/prettytime)
* [Material Search View](https://github.com/MiguelCatalan/MaterialSearchView)
* [Google Play Services: Map](https://developers.google.com/maps/documentation/android-api/start?hl=es-419)
* [Facebook SDK](https://developers.facebook.com/docs/android/)

## How Can I Contribute?

### Development Workflow

In order to work on this projects it is necessary to review and adopt the steps specified on the [Workflow Document](/WORKFLOW.md)

### Reporting Bugs

This section guides you through submitting a bug report. Following these guidelines helps maintainers and the development team understand any report made, reproduce the behavior and find related reports.

Before creating bug reports, please check [this list](#before-submitting-a-bug-report) as you might find out that you don't need to create one. When you are creating a bug report, please [include as many details as possible](#how-do-i-submit-a-good-bug-report). If you'd like, you can use [this template](#template-for-submitting-bug-reports) to structure the information.

#### Before Submitting A Bug Report

* **Make sure is a bug.** You might be able to find the cause of the problem and fix things yourself. Most importantly, check if you can reproduce the problem in the latest version of the app.
* **Perform a [search](https://gitlab/username/projectname/issues)** to see if the problem has already been reported. If it has, add a comment to the existing issue instead of opening a new one.

#### How Do I Submit A (Good) Bug Report?

Bugs are tracked as GitLab issues. After you've determined a bug, create an issue on the application repository and provide the following information.

Explain the problem and include additional details to help maintainers reproduce the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. For example, start by explaining how you started the Android App, e.g. which was the state of the network connection, gps or the device in general. When listing steps, **don't just say what you did, but explain how you did it**. For example, if you .., explain if you ..., and if so ...?
* **Provide specific examples to demonstrate the steps**. Include links to files, other projects, or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use [Markdown code blocks](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#code-and-syntax-highlighting).
* **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem. **Include this only if it is completely necessary.**
* **If you're reporting that App crashed**, include a crash report with a stack trace from the LogCat. Include the crash report in the issue in a [code block](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#code-and-syntax-highlighting).
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new version of a third party library) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in an older version of the app?** What's the most recent version in which the problem doesn't happen?

Include details about your configuration and environment:

* **Which version of app are you using?** You can get the exact version in `versionName` variable on the `app/build.gradle'` file.
* **What's the name and version of the OS you're using**?
* **Are you running an emulator?** If so, which one are you using?

#### Template For Submitting Bug Reports

    [Short description of problem here]

**Reproduction Steps:**

    1. [First Step]
    2. [Second Step]
    3. [Other Steps...]

**Expected behavior:**

    [Describe expected behavior here]

**Observed behavior:**

    [Describe observed behavior here]

**Screenshots and GIFs (Only if necessary)**

    ![Screenshots and GIFs which follow reproduction steps to demonstrate the problem](url)]

**App version:** [Enter App version here]

**Android OS and version: (Only if necessary)** [Enter OS name and version here]

**Additional information:**

* Problem can be easily reproduced: [Yes/No]
* Problem started happening recently, didn't happen in an older version of the App: [Yes/No]
* Problem can be reliably reproduced, doesn't happen randomly: [Yes/No]

### Using the GitLab board

#### Board Structure

The issue board has 5 main lists:
1. **Backlog:** All issues created by the Repository Administrator. This issues are not part of any iteration, could be features request but not analyzed yet, or bugs reported by the reviewers or the developers.
2. **Ready:** Approved issues for the next iteration stablished.
3. **Doing:** Current issues being worked on by developers.
4. **Needs Review:** Issues that needs a Administrator review and approval to be merged to the code base. This issues has a **Pull Request** linked.
5. **Done:** Issues that were merged to the code base. This issues are in a *Closed* state.

#### How to move issues along the board?

For now the board items should be moved manually. A team member could move a item depending on his role.

###### For Developers:
* From the *Ready* list, a developer can move an assigned issue to the *Doing* list.
* From the *Doing* list, a developer can move a worked on issue to the *Needs Review* list.

###### For Administrators:
* From the *Backlog* list, a developer can move an assigned issue to the *Ready* list.
* From the *Needs Review* list, a developer cam move a worked issue to the *Done* list.

### Pull Requests

  * Include screenshots and animated GIFs in your pull request whenever possible.
  * Follow the [Android Styleguide](#android-styleguide).
  * Document new code based on the
    [Documentation Styleguide](#documentation-styleguide)
  * About dependencies...
  * About class variables..
  * About methods..

## Styleguides

### Git Commit Messages

* Use the present tense:
  * "Add feature" :white_check_mark:
  * not "Added feature" :negative_squared_cross_mark:
* Use the imperative mood:
  * "Move cursor to..." :white_check_mark:
  * not "Moves cursor to..." :negative_squared_cross_mark:
* Limit the first line to 72 characters or less.
* Reference issues and pull requests liberally.

### Android Styleguide

For now, the project is not using a Styleguide reference for particular elements of the Android Framework. This section may be more relevant in the future. We encourage the developer to try to adopt previous conventions on naming resources or code files that already exists on the code base.

### Documentation Styleguide

For now, the project is using the standard Javadoc style for writing documentation on class and methods. We encourage the developer to research about this topic. However, here are some useful links:
* [A styleguide](https://github.com/kijiproject/wiki/wiki/Javadoc-Style-Guide)
* [Tutorials Poitn: Java Documentation](https://www.tutorialspoint.com/java/java_documentation.htm)
* [Oracle Documentation](http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html)

#### Example

  ```java
  /**
  * The HelloWorld program implements an application that
  * simply displays "Hello World!" to the standard output.
  *
  * @author  Juan Pérez
  * @version 1.0
  * @since   2016-12-12
  */
  public class HelloWorld {

      public static void main(String[] args) {
        //Prints Hello, World! on standard output.
        System.out.println("Hello World!");
      }

  }
  ```

## Additional Notes

### Issue and Pull Request Labels

This section lists the labels we use to help us track and manage issues and pull requests. Most labels are used across all company repositories, but some are specific to `/project-name`.

The labels are loosely grouped by their purpose, but it's not required that every issue have a label from every group or that an issue can't have more than one label from the same group.

#### Type of Issue and Issue State

| Label name | Description |
| :------: | ------ |
| [`feature`][search-repo-label-feature] | An application functionality that must be implemented. |
| [`enhancement`][search-repo-label-enhancement] | Feature requests. |
| [`bug`][search-repo-label-bug] | Confirmed bugs or reports that are very likely to be bugs. |
| [`chore`][search-repo-label-chore] | Technical tasks or configuration work in general |
| [`question`][search-repo-label-question] | Questions more than bug reports or feature requests (e.g. how do I do X). |
| [`feedback`][search-repo-label-feedback] | General feedback more than bug reports or feature requests. |
| [`more-information-needed`][search-repo-label-more-information-needed] | More information needs to be collected about these problems or feature requests (e.g. steps to reproduce). |
| [`needs-reproduction`][search-repo-label-needs-reproduction] | Likely bugs, but haven't been reliably reproduced. |
| [`duplicate`][search-repo-label-duplicate] | Issues which are duplicates of other issues, i.e. they have been reported before. |
| [`wontfix`][search-repo-label-wontfix] | The project administrator and the company administration has decided not to fix these issues for now, either because they're working as intended or for some other reason. |
| [`hotfix`][search-repo-label-hotfix] | Fix a bug detected on production or any released application version. |

#### Topic Categories

| Label name | Description |
| :------: | ------ |
| [`documentation`][search-repo-label-documentation] | Related to any type of documentation (e.g. [API documentation](http://docs.projectname.apiary.io/) or the application manual) |
| [`performance`][search-repo-label-performance] | Related to performance. |
| [`security`][search-repo-label-security] | Related to security. |
| [`ui/ux`][search-repo-label-ui-ux] | Related to visual design. |
| [`backend`][search-repo-label-backend] | Related to logic implementation. |
| [`api-client`][search-repo-label-api-client] | Related to API client on the app. |
| [`crash`][search-repo-label-crash] | Reports of completely crashing. |  
| [`git`][search-repo-label-git] | Related to Git functionality (e.g. problems with .gitignore files or with showing the correct file status). |

#### `project-name` Topic Categories

| Label name | Description |
| :------: | ------ |
| [`home`][search-repo-label-home] | Related to Home feature of the app. |
| [`login`][search-repo-label-login] | Related to Login feature of the app. |
| [`notifications`][search-repo-label-notifications] | Related to Notifications feature of the app. |
| [`user`][search-repo-label-user] | Related to any user options of the app. |
| [`location`][search-repo-label-location] | Related to any location implementation of the app. |
| [`geofence`][search-repo-label-geofence] | Related to the geofences implementation of the app. |
| [`history`][search-repo-label-history] | Related to the history implementation of the app. |
| [`profile`][search-repo-label-profile] | Related to a profile implementation of the app. |
| [`map`][search-repo-label-map] | Related to any map implementation of the app. |

#### Board Labels

| Label name | Description |
| :------: | ------ |
| [`backlog`][search-repo-label-backlog] | All issues created by the Repository Administrator. This issues are not part of any iteration, could be features request but not analyzed yet, or bugs reported by the reviewers or the developers. |
| [`ready`][search-repo-label-ready] | Approved issues for the next iteration stablished. |
| [`doing`][search-repo-label-doing] | Current issues being worked on by developers. |
| [`needs-review`][search-repo-label-needs-review] | Issues which need code review, and approval from the assignee. |
| [`done`][search-repo-label-done] | Issues that were merged to the code base. This issues are in a *Closed* state. |

#### Pull Request Labels

| Label name | Description
| :------: | ------ |
| [`work-in-progress`][search-repo-label-work-in-progress] | Pull requests which are still being worked on, more changes will follow. |  
| [`requires-changes`][search-repo-label-requires-changes] | Pull requests which need to be updated based on review comments and then reviewed again. |
| [`needs-testing`][search-repo-label-needs-testing] | Pull requests which need manual testing. |

[search-repo-label-feature]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=feature
[search-repo-label-enhancement]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=enhancement
[search-repo-label-bug]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=bug
[search-repo-label-chore]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=bug
[search-repo-label-question]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=question
[search-repo-label-feedback]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=feedback
[search-repo-label-more-information-needed]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=more+information+needed
[search-repo-label-needs-reproduction]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=needs+reproduction
[search-repo-label-duplicate]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=duplicate
[search-repo-label-wontfix]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=wontfix
[search-repo-label-hotfix]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=hotfix

[search-repo-label-documentation]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=documentation
[search-repo-label-performance]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=performance
[search-repo-label-security]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=security
[search-repo-label-backend]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=backend
[search-repo-label-ui-ux]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=ui%2Fux
[search-repo-label-api-client]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=api+client
[search-repo-label-crash]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=crash
[search-repo-label-git]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=git

[search-repo-label-home]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=home
[search-repo-label-login]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=login
[search-repo-label-notifications]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=notifications
[search-repo-label-pet-options]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=pet+options
[search-repo-label-user]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=user
[search-repo-label-location]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=location
[search-repo-label-geofence]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=geofence
[search-repo-label-history]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=history
[search-repo-label-profile]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=profile
[search-repo-label-map]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=map

[search-repo-label-backlog]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=backlog
[search-repo-label-ready]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=ready
[search-repo-label-doing]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=doing
[search-repo-label-needs-review]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=needs+review
[search-repo-label-done]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=done

[search-repo-label-work-in-progress]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=work+in+progress
[search-repo-label-requires-changes]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=requires+changes
[search-repo-label-needs-testing]:http://www.gitlab.com/username/projectname/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=needs+testing
