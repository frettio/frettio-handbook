#!/bin/bash

# Consider before run: chmod +x /path/to/script.sh
# How to run: ./listallprojects.sh PRIVATE-TOKEN

private_key=$1

url="https://gitlab.com/api/v3/projects/"
echo "Requesting to project with key "$private_key"... to URL "$url""

header=""PRIVATE-TOKEN:" "$private_key""

curlBody="--header \"$header\""
curlCmd="curl -v $curlBody $url"
eval "touch "listprojectssoutput.json""
eval $curlCmd >> listprojectssoutput.json